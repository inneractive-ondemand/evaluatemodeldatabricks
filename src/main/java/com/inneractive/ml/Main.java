package com.inneractive.ml;

import com.databricks.ml.local.LocalModel;
import com.databricks.ml.local.ModelImport;

public class Main {

    public static void main(String[]  args){
        LocalModel localModel = ModelImport.loadModel("/home/inneractive/dev/poc/inneractive-import-ml/model");

        String[] outputs = {/*"label",*/"prediction", "probability"};
        localModel.setOutputCols(outputs);

        String input = "{\"age\":"+18+",\"workclass\":\"Private\",\"fnlwgt\":"+309634+
                ",\"education\":\"11th\",\"education_num\":"+7+",\"marital_status\":\"Never-married\""+
                ",\"occupation\":\"Other-service\",\"relationship\":\"Own-child\",\"race\":\"White\""+
                ",\"sex\":\"Female\",\"capital_gain\":"+0+",\"capital_loss\":"+0+",\"hours_per_week\":"+22+
                ",\"native_country\":\"United-States\"}";

        String input2 = "{\"age\":"+52+",\"workclass\":\"Self-emp-not-inc\",\"fnlwgt\":"+209642+
                ",\"education\":\"HS-grad\",\"education_num\":"+9+",\"marital_status\":\"Married-civ-spouse\""+
                ",\"occupation\":\"Exec-managerial\",\"relationship\":\"Husband\",\"race\":\"White\""+
                ",\"sex\":\"Male\",\"capital_gain\":"+0+",\"capital_loss\":"+0+",\"hours_per_week\":"+45+
                ",\"native_country\":\"United-States\"}";

        String output = localModel.transform(input);
        System.out.println( "OUTPUT: " + output );

        String output2 = localModel.transform(input2);
        System.out.println( "OUTPUT2: " + output2 );

    }
}

/*
 OUTPUT: {"prediction":0.0,"probability":{"type":1,"values":[0.5619418138090041,0.4380581861909959]}}
OUTPUT2: {"prediction":1.0,"probability":{"type":1,"values":[0.2445358603310937,0.7554641396689064]}}
 */